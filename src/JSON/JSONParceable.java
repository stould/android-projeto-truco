package JSON;

import java.util.List;

/**
 * 
 * @author viniicius
 * @see This Interface represets the parceable object.
 */
public interface JSONParceable {
	public List<?> parse(String jsonstr);
}
