package JSON;

import java.util.ArrayList;
import java.util.List;

import models.RankingModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @author viniicius
 * @see This class parse JSON to show the top 30 ranking.
 */
public class JSONParserRanking {

	/**
	 * @see The parameters to be parced with the Java JSONParcer.
	 */
	private static final String DATA = "data";
	private static final String NICK = "nick";
	private static final String SCORE = "score";
	private static final String LASTMATCH = "lastmatch";

	/**
	 * This method parse the json received from the server.
	 * 
	 * @param json
	 * @return List of RankingModel
	 */
	public List<RankingModel> parse(String jsonstr) {
		List<RankingModel> list = new ArrayList<RankingModel>();
		try {
			JSONObject json = new JSONObject(jsonstr);
			JSONArray data = json.getJSONArray(DATA);
			for (int i = 0; i < data.length(); i++) {
				JSONObject c = data.getJSONObject(i);
				RankingModel g = new RankingModel();
				g.nick = c.getString(NICK);
				g.score = c.getInt(SCORE);
				g.lastMatch = c.getString(LASTMATCH);
				list.add(g);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return list;
	}
}