package JSON;

import java.util.ArrayList;
import java.util.List;

import models.LastGamesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * 
 * @author viniicius
 * @see This class parse JSON to show the last 15 games.
 */
public class JSONParserLastGames implements JSONParceable {

	/**
	 * @see The parameters to be parced with the Java JSONParcer.
	 */
	private static final String DATA = "data";
	private static final String TIMEA = "timea";
	private static final String TIMEB = "timeb";
	private static final String TEAMAPOINTS = "teamapoints";
	private static final String TEAMBPOINTS = "teambpoints";

	/**
	 * This method parse the json received from the server.
	 * 
	 * @param json
	 * @return List of LastGamesModel
	 */
	@Override
	public List<LastGamesModel> parse(String jsonstr) {
		List<LastGamesModel> list = new ArrayList<LastGamesModel>();
		try {
			JSONObject json = new JSONObject(jsonstr);
			JSONArray data = json.getJSONArray(DATA);
			for (int i = 0; i < data.length(); i++) {
				LastGamesModel game = new LastGamesModel();
				JSONArray teama = new JSONArray(data.getJSONObject(i).getString(TIMEA));
				game.time_a_p1 = teama.getString(0);
				game.time_a_p2 = teama.getString(1);
				JSONArray teamb = new JSONArray(data.getJSONObject(i).getString(TIMEB));
				game.time_b_p1 = teamb.getString(0);
				game.time_b_p2 = teamb.getString(1);
				game.score_time_a = data.getJSONObject(i).getInt(TEAMAPOINTS);
				game.score_time_b = data.getJSONObject(i).getInt(TEAMBPOINTS);
				list.add(game);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return list;
	}
}