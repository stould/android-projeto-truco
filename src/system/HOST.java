package system;

import java.net.UnknownHostException;

import android.util.Log;

/**
 * 
 * @author viniicius
 * @see This class represents the HOST of the server.
 */
public final class HOST {
	private final static String HOST = "http://192.168.0.100";

	public static final String getHost() {

		return HOST;
	}

	public static final void test() {
		try {
			final String IP = java.net.Inet4Address.getLocalHost().getHostAddress();
			Log.i("IP", IP);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
