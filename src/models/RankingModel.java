package models;

/**
 * 
 * @author viniicius
 * @see This class is a model of the most winner players.
 */
public class RankingModel {
	public String nick;
	public String lastMatch;
	public int score;
}
