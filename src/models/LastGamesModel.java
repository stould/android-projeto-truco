package models;

/**
 * 
 * @author viniicius
 * @see This class is a model of the last games happened.
 */
public class LastGamesModel {
	public String time_a_p1;
	public String time_a_p2;
	public String time_b_p1;
	public String time_b_p2;
	public int score_time_a;
	public int score_time_b;
}
