package projeto_truco_activitys;

import java.util.ArrayList;
import java.util.List;

import models.LastGamesModel;
import system.HOST;
import system.JHTTPConnection;
import JSON.JSONParserLastGames;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.andrid_projetotruco.R;

/**
 * 
 * @author viniicius
 * @see This class represents the activity the last games happened.
 */
public class LastGamesActivity extends Activity {
	private ListView listView;
	private JHTTPConnection http;
	private JSONParserLastGames json;
	private final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			setList();
		}
	};
	private String req = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lastgames);
		listView = (ListView) findViewById(R.id.listViewLastGames);
		http = new JHTTPConnection();
		json = new JSONParserLastGames();

		// Set the margin in percents {
		int heigth = getWindowManager().getDefaultDisplay().getHeight();
		int starting = (heigth * 28) / 100;
		LinearLayout.LayoutParams llp = (android.widget.LinearLayout.LayoutParams) listView.getLayoutParams();
		llp.setMargins(0, starting, 0, 0);
		listView.setLayoutParams(llp);
		// }

		Thread th = new Thread(new Runnable() {
			@Override
			public void run() {
				req = http.makeHttpRequest(HOST.getHost() + "/getLastGames.php", "GET", http.addParams(new String[] {}, new String[] {}));
				handler.sendEmptyMessage(0);
			}
		});
		th.start();
	}

	public void setList() {
		List<LastGamesModel> list_ = json.parse(req);
		listView.setAdapter(new ListViewLastGames((ArrayList<LastGamesModel>) list_, this));
	}

	/**
	 * 
	 * @author viniicius
	 * @see This class represents the BaseAdapter of an game model.
	 */
	private class ListViewLastGames extends BaseAdapter {
		private ArrayList<LastGamesModel> _data;
		Context _c;

		public ListViewLastGames(ArrayList<LastGamesModel> data, Context c) {
			_data = data;
			_c = c;
		}

		public int getCount() {
			return _data.size();
		}

		public Object getItem(int position) {
			return _data.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) _c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(com.example.andrid_projetotruco.R.layout.rowlastgames, null);
			}

			TextView teamA = (TextView) v.findViewById(com.example.andrid_projetotruco.R.id.rowLastGamesTeam);

			LastGamesModel game = _data.get(position);
			teamA.setText(game.time_a_p1 + " e " + game.time_a_p2 + ": " + game.score_time_a + "pts\n" + game.time_b_p1 + " e " + game.time_b_p2 + ": " + game.score_time_b + "pts");
			return v;
		}
	}

}