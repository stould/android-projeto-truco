package projeto_truco_activitys;

import java.util.HashSet;
import java.util.Set;

import system.HOST;
import system.JHTTPConnection;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andrid_projetotruco.R;

/**
 * 
 * @author viniicius
 * @see This class represets the activity to confirm the autenticity of the four
 *      players.
 */
public class ConfirmLoginActivity extends Activity {

	private JHTTPConnection http;
	private String endGame = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirm_login);

		// Set the margin in percents {
		TextView tv = (TextView) findViewById(R.id.labelDotimeA);
		int heigth = getWindowManager().getDefaultDisplay().getHeight();
		int starting = (heigth * 28) / 100;
		LinearLayout.LayoutParams llp = (android.widget.LinearLayout.LayoutParams) tv.getLayoutParams();
		llp.setMargins(0, starting, 0, 0);
		tv.setLayoutParams(llp);
		// }

		http = new JHTTPConnection();
		endGame = getIntent().getStringExtra("endGame");
		if (endGame != null) {
			((TextView) findViewById(R.id.labelDotimeA)).setText("Ganhadores:");
			((TextView) findViewById(R.id.labelDotimeB)).setText("Perdedores:");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.confirm_login, menu);
		return true;
	}

	/**
	 * This method confirm the login. If the game is done, confirm the login and
	 * update the ranking. Else go to game screen.
	 * 
	 * @param view
	 */
	public void confirmLogin(View v) {
		EditText login_teama_p1 = (EditText) findViewById(R.id.loginteamap1);
		EditText login_teama_p2 = (EditText) findViewById(R.id.loginteamap2);
		EditText senha_teama_p1 = (EditText) findViewById(R.id.passteamap1);
		EditText senha_teama_p2 = (EditText) findViewById(R.id.passteamap2);

		EditText login_teamb_p1 = (EditText) findViewById(R.id.loginteambp1);
		EditText login_teamb_p2 = (EditText) findViewById(R.id.loginteambp2);
		EditText senha_teamb_p1 = (EditText) findViewById(R.id.passteambp1);
		EditText senha_teamb_p2 = (EditText) findViewById(R.id.passteambp2);

		Set<String> logins = new HashSet<String>();
		logins.add(login_teama_p1.getText().toString());
		logins.add(login_teama_p2.getText().toString());
		logins.add(login_teamb_p1.getText().toString());
		logins.add(login_teamb_p2.getText().toString());

		if (logins.size() == 4) {
			String[] paramName = new String[] { "teamap1login", "teamap2login", "teamap1pass", "teamap2pass", "teambp1login", "teambp2login", "teambp1pass", "teambp2pass" };
			String[] paramValue = new String[] { login_teama_p1.getText().toString(), login_teama_p2.getText().toString(), senha_teama_p1.getText().toString(), senha_teama_p2.getText().toString(),
					login_teamb_p1.getText().toString(), login_teamb_p2.getText().toString(), senha_teamb_p1.getText().toString(), senha_teamb_p2.getText().toString() };

			String request = http.makeHttpRequest(HOST.getHost() + "/confirmLogin.php", "GET", http.addParams(paramName, paramValue));

			if (request.contains("OK")) {
				// In case of someone win the game.
				if (endGame != null) {

					String[] teamA = new String[4];
					String[] teamB = new String[4];
					teamA[0] = getIntent().getStringExtra("login_teama_p1");
					teamA[1] = getIntent().getStringExtra("senha_teama_p1");
					teamA[2] = getIntent().getStringExtra("login_teama_p2");
					teamA[3] = getIntent().getStringExtra("senha_teama_p2");

					teamB[0] = getIntent().getStringExtra("login_teamb_p1");
					teamB[1] = getIntent().getStringExtra("senha_teamb_p1");
					teamB[2] = getIntent().getStringExtra("login_teamb_p2");
					teamB[3] = getIntent().getStringExtra("senha_teamb_p2");

					Log.i("TimeA", teamA[0] + " e " + teamA[2]);
					Log.i("TimeB", teamB[0] + " e " + teamB[2]);

					int teamWinner = getIntent().getIntExtra("winner", -1);
					Log.i("Winner", teamWinner + "");
					boolean wrongLogins = true;
					if (teamWinner == 1) {
						String ganhador1 = teamA[0];
						String ganhador2 = teamA[2];
						String loginjog1 = login_teama_p1.getText().toString();
						String loginjog2 = login_teama_p2.getText().toString();
						if ((loginjog1.equals(ganhador1) && loginjog2.equals(ganhador2)) || (loginjog1.equals(ganhador2) && loginjog2.equals(ganhador1))) {
							loginjog1 = login_teamb_p1.getText().toString();
							loginjog2 = login_teamb_p2.getText().toString();
							String perdedor1 = teamB[0];
							String perdedor2 = teamB[2];
							if ((loginjog1.equals(perdedor1) && loginjog2.equals(perdedor2)) || (loginjog1.equals(perdedor2) && loginjog2.equals(perdedor1))) {
								wrongLogins = false;
							}
						}
					} else {
						String ganhador1 = teamB[0];
						String ganhador2 = teamB[2];
						String loginjog1 = login_teama_p1.getText().toString();
						String loginjog2 = login_teama_p2.getText().toString();
						if ((loginjog1.equals(ganhador1) && loginjog2.equals(ganhador2)) || (loginjog1.equals(ganhador2) && loginjog2.equals(ganhador1))) {
							loginjog1 = login_teamb_p1.getText().toString();
							loginjog2 = login_teamb_p2.getText().toString();
							String perdedor1 = teamA[0];
							String perdedor2 = teamA[2];
							if ((loginjog1.equals(perdedor1) && loginjog2.equals(perdedor2)) || (loginjog1.equals(perdedor2) && loginjog2.equals(perdedor1))) {
								wrongLogins = false;
							}
						}
					}
					AlertDialog.Builder dialog = new AlertDialog.Builder(this);
					if (wrongLogins) {
						dialog.create();
						dialog.setTitle("Message");
						dialog.setMessage("Os ganhadores devem colocar obrigatoriamente login/senha no time dos ganhadores.");
						dialog.show();
					} else {
						String score = getIntent().getStringExtra("score");
						paramName = new String[] { "login1", "login2", "login3", "login4", "passp1", "passp2", "game" };
						if (teamWinner == 1) {
							paramValue = new String[] { teamA[0], teamA[2], teamB[0], teamB[2], teamA[1], teamB[3], score };
						} else {
							paramValue = new String[] { teamB[0], teamB[2], teamA[0], teamA[2], teamB[1], teamA[3], score };
						}
						request = http.makeHttpRequest(HOST.getHost() + "/updatePoints.php", "GET", http.addParams(paramName, paramValue));
						if (request.contains("OK")) {
							dialog.create();
							dialog.setTitle("Message");
							dialog.setMessage("Jogo registrado com sucesso.");
							dialog.show();
							dialog = null;
							finish();
						} else {
							dialog.create();
							dialog.setTitle("Message");
							dialog.setMessage("Erro - Alguem n�o digitou corretamente o nick e/ou a senha." + request);
							dialog.show();
							dialog = null;
						}
					}
				} else {
					Log.i("TimeA", login_teama_p1.getText().toString() + " e " + login_teama_p2.getText().toString());
					Log.i("TimeB", login_teamb_p1.getText().toString() + " e " + login_teamb_p2.getText().toString());
					Intent intent = new Intent(this, GameAcivity.class);
					intent.putExtra("login_teama_p1", login_teama_p1.getText().toString());
					intent.putExtra("login_teama_p2", login_teama_p2.getText().toString());
					intent.putExtra("login_teamb_p1", login_teamb_p1.getText().toString());
					intent.putExtra("login_teamb_p2", login_teamb_p2.getText().toString());

					intent.putExtra("senha_teama_p1", senha_teama_p1.getText().toString());
					intent.putExtra("senha_teama_p2", senha_teama_p2.getText().toString());
					intent.putExtra("senha_teamb_p1", senha_teamb_p1.getText().toString());
					intent.putExtra("senha_teamb_p2", senha_teamb_p2.getText().toString());
					startActivity(intent);
					finish();
				}
			} else {
				Toast.makeText(this, "Erro - Alguem n�o digitou corretamente o nick e/ou a senha.", Toast.LENGTH_LONG).show();
			}
		} else {
			Toast.makeText(this, "� necess�rio quatro jogadores para uma partida.", Toast.LENGTH_LONG).show();
		}
	}
}
