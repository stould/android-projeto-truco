package projeto_truco_activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.andrid_projetotruco.R;

/**
 * 
 * @author viniicius
 * @see This class represents the activity of an game of truco.
 */
public class GameAcivity extends Activity {
	private int scorea, scoreb;
	private TextView tv_teama;
	private TextView tv_teamb;

	private TextView tv_scoreteama;
	private TextView tv_scoreteamb;
	private String[] teamA = new String[4];
	private String[] teamB = new String[4];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_acivity);
		scorea = scoreb = 0;
		tv_teama = (TextView) findViewById(R.id.teama);
		tv_teamb = (TextView) findViewById(R.id.teamb);

		// Set the margin in percents {
		LinearLayout ll = (LinearLayout) findViewById(R.id.gameMainLayout);
		int heigth = getWindowManager().getDefaultDisplay().getHeight();
		int starting = (heigth * 28) / 100;
		LinearLayout.LayoutParams llp = (android.widget.LinearLayout.LayoutParams) ll.getLayoutParams();
		llp.setMargins(0, starting, 0, 0);
		ll.setLayoutParams(llp);
		// }

		tv_scoreteama = (TextView) findViewById(R.id.scoreteama);
		tv_scoreteamb = (TextView) findViewById(R.id.scoreteamb);
		teamA[0] = getIntent().getStringExtra("login_teama_p1");
		teamA[1] = getIntent().getStringExtra("senha_teama_p1");
		teamA[2] = getIntent().getStringExtra("login_teama_p2");
		teamA[3] = getIntent().getStringExtra("senha_teama_p2");

		teamB[0] = getIntent().getStringExtra("login_teamb_p1");
		teamB[1] = getIntent().getStringExtra("senha_teamb_p1");
		teamB[2] = getIntent().getStringExtra("login_teamb_p2");
		teamB[3] = getIntent().getStringExtra("senha_teamb_p2");

		tv_teama.setText(teamA[0] + " e " + teamA[2]);
		tv_teamb.setText(teamB[0] + " e " + teamB[2]);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_acivity, menu);
		return true;
	}

	/**
	 * This method upodate the score of a team.
	 * 
	 * @param n
	 * @param score
	 * @param team
	 * @return int
	 */
	private int normalize(int n, int score, int team) {
		int ans = 0;
		if (score + n > 12) {
			ans = 12;
		} else {
			ans = score + n;
		}
		if (team == 1) {
			tv_scoreteama.setText("[ " + Integer.toString(ans) + " ]");
		} else {
			tv_scoreteamb.setText("[ " + Integer.toString(ans) + " ]");
		}
		return ans;
	}

	/**
	 * This method verify if a game was done, if true, send the score to the
	 * database.
	 * 
	 * @param score
	 * @param team
	 * @return void
	 */
	private void matchVictory(int score, int team) {
		if (score >= 12) {
			Intent intent = new Intent(this, ConfirmLoginActivity.class);
			intent.putExtra("login_teama_p1", teamA[0]);
			intent.putExtra("login_teama_p2", teamA[2]);
			intent.putExtra("login_teamb_p1", teamB[0]);
			intent.putExtra("login_teamb_p2", teamB[2]);

			intent.putExtra("senha_teama_p1", teamA[1]);
			intent.putExtra("senha_teama_p2", teamA[3]);
			intent.putExtra("senha_teamb_p1", teamB[1]);
			intent.putExtra("senha_teamb_p2", teamB[3]);

			intent.putExtra("winner", team);
			intent.putExtra("endGame", "OK");
			intent.putExtra("score", Integer.toString(scorea) + "_" + Integer.toString(scoreb));
			startActivity(intent);
			finish();
		}
	}

	// Team A
	public void addUm_a(View v) {
		scorea = normalize(1, scorea, 1);
		matchVictory(scorea, 1);
	}

	public void addTres_a(View v) {
		scorea = normalize(3, scorea, 1);
		matchVictory(scorea, 1);
	}

	public void addSeis_a(View v) {
		scorea = normalize(6, scorea, 1);
		matchVictory(scorea, 1);
	}

	public void addNove_a(View v) {
		scorea = normalize(9, scorea, 1);
		matchVictory(scorea, 1);
	}

	public void addDoze_a(View v) {
		scorea = normalize(12, scorea, 1);
		matchVictory(scorea, 1);
	}

	// Division- Team B

	public void addUm_b(View v) {
		scoreb = normalize(1, scoreb, 2);
		matchVictory(scoreb, 2);
	}

	public void addTres_b(View v) {
		scoreb = normalize(3, scoreb, 2);
		matchVictory(scoreb, 2);
	}

	public void addSeis_b(View v) {
		scoreb = normalize(6, scoreb, 2);
		matchVictory(scoreb, 2);
	}

	public void addNove_b(View v) {
		scoreb = normalize(9, scoreb, 2);
		matchVictory(scoreb, 2);
	}

	public void addDoze_b(View v) {
		scoreb = normalize(12, scoreb, 2);
		matchVictory(scoreb, 2);
	}
}
