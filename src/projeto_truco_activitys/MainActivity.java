package projeto_truco_activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.andrid_projetotruco.R;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Set the margin in percents {
		com.google.android.apps.iosched.ui.widget.DashboardLayout ll = (com.google.android.apps.iosched.ui.widget.DashboardLayout) findViewById(R.id.main_dashboard);
		int heigth = getWindowManager().getDefaultDisplay().getHeight();
		int starting = (heigth * 28) / 100;
		LinearLayout.LayoutParams llp = (android.widget.LinearLayout.LayoutParams) ll.getLayoutParams();
		llp.setMargins(0, starting, 0, 0);
		ll.setLayoutParams(llp);
		// }

		if (!isOnline(getApplicationContext())) {
			Toast.makeText(this, "E necessario que conexao com a internet para que a aplicacao funcione corretamente.", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void lastGames(View v) {
		Intent intent = new Intent(this, LastGamesActivity.class);
		startActivity(intent);
	}

	public void appDetails(View v) {
		Intent intent = new Intent(this, AppdetailsActivity.class);
		startActivity(intent);
	}

	public void playGame(View v) {
		Intent intent = new Intent(this, ConfirmLoginActivity.class);
		startActivity(intent);
	}

	public void howToPlay(View v) {
		Intent intent = new Intent(this, HowToPlayActivity.class);
		startActivity(intent);
	}

	public void ranking(View v) {
		Intent intent = new Intent(this, RankingActivity.class);
		startActivity(intent);
	}

	public void register(View v) {
		Intent intent = new Intent(this, RegisterActivity.class);
		startActivity(intent);
	}

	public static boolean isOnline(Context contexto) {
		ConnectivityManager cm = (ConnectivityManager) contexto.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if ((netInfo != null) && (netInfo.isConnectedOrConnecting()) && (netInfo.isAvailable()))
			return true;
		return false;
	}
}
