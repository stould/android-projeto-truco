package projeto_truco_activitys;

import java.util.ArrayList;
import java.util.List;

import models.RankingModel;
import system.HOST;
import system.JHTTPConnection;
import JSON.JSONParserRanking;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.andrid_projetotruco.R;

/**
 * 
 * @author viniicius
 * @see This class represents the activity the last games happened.
 */
public class RankingActivity extends Activity {
	private ListView listView;
	private JHTTPConnection http;
	private JSONParserRanking json;
	private final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			setList();
		}
	};
	private String req = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ranking);
		listView = (ListView) findViewById(R.id.listViewRanking);
		http = new JHTTPConnection();
		json = new JSONParserRanking();

		// Set the margin in percents {
		int heigth = getWindowManager().getDefaultDisplay().getHeight();
		int starting = (heigth * 28) / 100;
		LinearLayout.LayoutParams llp = (android.widget.LinearLayout.LayoutParams) listView.getLayoutParams();
		llp.setMargins(0, starting, 0, 0);
		listView.setLayoutParams(llp);
		// }

		Thread th = new Thread(new Runnable() {
			@Override
			public void run() {
				req = http.makeHttpRequest(HOST.getHost() + "/getRank.php", "GET", http.addParams(new String[] {}, new String[] {}));
				handler.sendEmptyMessage(0);
			}
		});
		th.start();
	}

	public void setList() {
		List<RankingModel> list_ = json.parse(req);
		listView.setAdapter(new ListViewRanking((ArrayList<RankingModel>) list_, this));
	}

	/**
	 * 
	 * @author viniicius
	 * @see This class represents the BaseAdapter of an game model.
	 */
	private class ListViewRanking extends BaseAdapter {
		private ArrayList<RankingModel> _data;
		Context _c;

		public ListViewRanking(ArrayList<RankingModel> data, Context c) {
			_data = data;
			_c = c;
		}

		public int getCount() {
			return _data.size();
		}

		public Object getItem(int position) {
			return _data.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) _c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(com.example.andrid_projetotruco.R.layout.rowranking, null);
			}

			TextView player = (TextView) v.findViewById(com.example.andrid_projetotruco.R.id.rowRanking);

			RankingModel model = _data.get(position);
			player.setText("Nick: " + model.nick + "\n" + "Pontos: " + model.score + "\n" + "Ultima partida: " + model.lastMatch.replace("-", "/"));
			return v;
		}
	}

}