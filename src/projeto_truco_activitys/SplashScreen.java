package projeto_truco_activitys;

import com.example.andrid_projetotruco.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;

/**
 * 
 * @author viniicius
 * @see This class represents initial screen to be displayed for the user.
 */
public class SplashScreen extends Activity implements Runnable, OnClickListener {

	private static int FRAME_TIME = 4000;
	private static int TOTAL_TIME = FRAME_TIME;
	private ViewFlipper viewFlipper;
	private Animation inAnimation;
	private Animation outAnimation;
	private Handler handler = new Handler();
	private boolean started;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		viewFlipper = (ViewFlipper) findViewById(R.id.flipper);
		viewFlipper.setOnClickListener(this);
		inAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_alpha_in);
		outAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_alpha_out);
		viewFlipper.setInAnimation(inAnimation);
		viewFlipper.setOutAnimation(outAnimation);
		viewFlipper.setFlipInterval(FRAME_TIME);
		viewFlipper.startFlipping();
		handler.postDelayed(this, TOTAL_TIME);
	}

	public void run() {
		iniciarPrograma();
	}

	public void onClick(View v) {
		iniciarPrograma();
	}

	private synchronized void iniciarPrograma() {
		while (true) {
			if (!started) {
				started = true;
				viewFlipper.stopFlipping();
				Intent i = new Intent(SplashScreen.this, MainActivity.class);
				startActivity(i);
				setResult(RESULT_OK);
				finish();
				break;
			}
		}
	}
}