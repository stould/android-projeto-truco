package projeto_truco_activitys;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.andrid_projetotruco.R;

/**
 * 
 * @author viniicius
 * @see This class represets the activity to show details of the application.
 */
public class AppdetailsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_appdetails);

		// Set the margin in percents {
		TextView tv = (TextView) findViewById(R.id.labelappdetails);
		int heigth = getWindowManager().getDefaultDisplay().getHeight();
		int starting = (heigth * 28) / 100;
		LinearLayout.LayoutParams llp = (android.widget.LinearLayout.LayoutParams) tv.getLayoutParams();
		llp.setMargins(12, starting, 12, 12);
		Log.i("WIDTH:", tv.getWidth() + "");
		int padd = (heigth - (68 + 24)) * 12 / 100;
		tv.setPadding(padd, tv.getPaddingTop(), padd, tv.getPaddingBottom());
		tv.setLayoutParams(llp);
		// }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.appdetails, menu);
		return true;
	}

}
