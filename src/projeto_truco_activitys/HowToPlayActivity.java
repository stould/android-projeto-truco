package projeto_truco_activitys;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.andrid_projetotruco.R;

public class HowToPlayActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_how_to_play);

		// Set the margin in percents {
		TextView ll = (TextView) findViewById(R.id.firstTxtViewHowToPlay);
		int heigth = getWindowManager().getDefaultDisplay().getHeight();
		int starting = (heigth * 28) / 100;
		LinearLayout.LayoutParams llp = (android.widget.LinearLayout.LayoutParams) ll.getLayoutParams();
		llp.setMargins(0, starting, 0, 0);
		ll.setLayoutParams(llp);
		// }

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.how_to_play, menu);
		return true;
	}

}
