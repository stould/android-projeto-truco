package projeto_truco_activitys;

import system.HOST;
import system.JHTTPConnection;
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;

import com.example.andrid_projetotruco.R;

public class RegisterActivity extends Activity {
	private JHTTPConnection http;
	private String req = "";
	private final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			sendMessageToUser(req);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		http = new JHTTPConnection();
		// Set the margin in percents {
		TableLayout ll = (TableLayout) findViewById(R.id.tableLayoutregister);
		int heigth = getWindowManager().getDefaultDisplay().getHeight();
		int starting = (heigth * 28) / 100;
		LinearLayout.LayoutParams llp = (android.widget.LinearLayout.LayoutParams) ll.getLayoutParams();
		llp.setMargins(0, starting, 0, 0);
		ll.setLayoutParams(llp);
		// }

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

	public void registerPlayer(View v) {
		final String nick = ((EditText) findViewById(R.id.registernick)).getText().toString();
		final String password = ((EditText) findViewById(R.id.registerpass)).getText().toString();
		Thread th = new Thread(new Runnable() {
			@Override
			public void run() {
				req = http.makeHttpRequest(HOST.getHost() + "/register.php", "GET", http.addParams(new String[] { "nick", "password" }, new String[] { nick, password }));
				handler.sendEmptyMessage(0);
			}
		});
		th.start();
	}

	public void sendMessageToUser(String req) {
		if (req.contains("OK")) {
			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.create();
			dialog.setTitle("Message");
			dialog.setMessage("Registrado com sucesso.");
			dialog.show();
			dialog = null;
			finish();
		} else {
			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.create();
			dialog.setTitle("Message");
			dialog.setMessage("Ja existe um jogador com este nick.");
			dialog.show();
			dialog = null;
		}
	}
}
